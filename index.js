const express = require("express");
const bodyParser = require('body-parser');
const productsRouter = require('./routes/products');

const app = express();

app.use(bodyParser.json());

app.use("/api/products", productsRouter);

/*app.get('/', (req, res) => {
  res.send({ saludo: "hola" });
});*/

const server = app.listen(3000, () => {
  console.log(`Listening http://localhost:${server.address().port}`);
})